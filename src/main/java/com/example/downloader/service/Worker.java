package com.example.downloader.service;

import com.example.downloader.model.Task;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.concurrent.BlockingDeque;

@RequiredArgsConstructor
@Slf4j
public class Worker implements Runnable {

    private final BlockingDeque<Task> queue;
    private final OkHttpClient httpClient = new OkHttpClient();

    @Override
    public void run() {
        try {
            for (;;) {
                final Task task = queue.takeFirst();
                process(task);
            }
        } catch (InterruptedException ex) {
            log.warn("Thread {} stopped", Thread.currentThread().getId());
            Thread.currentThread().interrupt();
        }
    }



    private void process(final @NotNull Task task) {
        try {
            log.info("Thread {} got task {} with url {}", Thread.currentThread().getId(), task.getId(), task.getUrl());

            task.setStatus(Task.Status.PENDING);
            download(task);
            task.setStatus(Task.Status.COMPLETED);
        } catch (IOException ex) {
            log.error(ex.getMessage());

            task.setStatus(Task.Status.ERROR);
        }
    }

    private void download(final @NotNull Task task) throws IOException {
        final Request request = new Request.Builder().url(task.getUrl()).build();
        try (final Response response = httpClient.newCall(request).execute()) {
            final String body = response.body() != null ? response.body().string() : "";
            long len = response.body() != null ? response.body().contentLength() : 0L;
            task.setResponseBody(body);
            task.setResponseHttpStatus(response.code());
            task.setResponseContentLength(len);
        }
    }
}
