package com.example.downloader.service;

import com.example.downloader.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
public class Downloader {

    private final AtomicLong nextID = new AtomicLong(0L);

    private final ExecutorService pool;

    private final BlockingDeque<Task> queue;
    private final List<Task> allTasks = new ArrayList<>();
    private final Lock mutex = new ReentrantLock(true);

    @Autowired
    public Downloader(@Value("${app.service.max_queued_tasks}") int maxQueued) {
        this.pool = Executors.newFixedThreadPool(maxQueued);
        this.queue = new LinkedBlockingDeque<>();

        for (int i = 0; i < maxQueued; ++i) {
            pool.execute(new Worker(queue));
        }
    }

    @PreDestroy
    public void destroy() {
        pool.shutdown();
    }

    public Long enqueue(final String url) {
        long id = nextID.addAndGet(1L);
        final Task task = new Task(id, url);
        queue.addLast(task);

        mutex.lock();
        try {
            allTasks.add(task);
        } finally {
            mutex.unlock();
        }

        return id;
    }

    public List<Task> last(long n) {
        mutex.lock();
        try {
            int size = allTasks.size();
            return allTasks.stream().skip(size - Math.min(size, n)).collect(Collectors.toList());
        } finally {
            mutex.unlock();
        }
    }

    public Optional<Task> get(long id) {
        mutex.lock();
        try {
            return Optional.ofNullable(id <= 0 || id > allTasks.size() ? null : allTasks.get((int) id - 1));
        } finally {
            mutex.unlock();
        }
    }
}
