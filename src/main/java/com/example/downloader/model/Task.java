package com.example.downloader.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Synchronized;

public class Task {
    public enum Status {
        NEW,
        PENDING,
        COMPLETED,
        ERROR
    }

    @JsonProperty(value = "ID")
    @Getter
    private Long id;

    @JsonProperty(value = "URL")
    @Getter
    private String url;

    private Status status = Status.NEW;

    private Long responseContentLength;

    private Integer responseHttpStatus;

    private String responseBody;

    public Task(long id, String url) {
        this.id = id;
        this.url = url;
    }

    @Synchronized
    @JsonProperty(value = "Status")
    public Status getStatus() { return status; }

    @Synchronized
    public void setStatus(Status status) { this.status = status; }

    @Synchronized
    @JsonProperty(value = "Response content length")
    public Long getResponseContentLength() { return responseContentLength; }

    @Synchronized
    public void setResponseContentLength(long contentLen) { this.responseContentLength = contentLen; }

    @Synchronized
    @JsonProperty(value = "Response HTTP status")
    public Integer getResponseHttpStatus() { return responseHttpStatus; }

    @Synchronized
    public void setResponseHttpStatus(int status) { this.responseHttpStatus = status; }

    @Synchronized
    @JsonProperty(value = "Response body")
    public String getResponseBody() { return responseBody; }

    @Synchronized
    public void setResponseBody(String body) { this.responseBody = body; }
}
