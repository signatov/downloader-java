package com.example.downloader.exception;

public class TaskNotFound extends Exception {
    public TaskNotFound(long id) {
        super(String.format("Task %d was not found", id));
    }
}
