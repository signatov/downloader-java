package com.example.downloader.controller;

import com.example.downloader.dto.ErrorResponse;
import com.example.downloader.dto.TaskID;
import com.example.downloader.dto.TaskURL;
import com.example.downloader.exception.TaskNotFound;
import com.example.downloader.model.Task;
import com.example.downloader.service.Downloader;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class DownloadController {

    private final Downloader downloader;

    @Value("${app.controller.max_shown_results}")
    private Long maxShownResults;

    @PostMapping(path = "/send")
    public ResponseEntity<TaskID> send(@RequestBody TaskURL data) {
        return new ResponseEntity<>(new TaskID(downloader.enqueue(data.getUrl())), HttpStatus.CREATED);
    }

    @GetMapping(path = "/result")
    public ResponseEntity<List<Task>> result(@RequestParam(value = "ID", required = false) Long id) throws TaskNotFound {
        if (id != null) {
            final Task task = downloader.get(id).orElseThrow(() -> new TaskNotFound(id));
            return new ResponseEntity<>(Collections.singletonList(task), HttpStatus.OK);
        }
        return new ResponseEntity<>(downloader.last(maxShownResults), HttpStatus.OK);
    }

    @ExceptionHandler(TaskNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponse handleNotFound(TaskNotFound ex) {
        return new ErrorResponse(ex.getMessage());
    }
}
