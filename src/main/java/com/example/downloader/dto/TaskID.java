package com.example.downloader.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TaskID {
    @JsonProperty(value = "ID")
    @Getter private final Long id;
}
