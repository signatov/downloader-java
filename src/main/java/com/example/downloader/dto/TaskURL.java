package com.example.downloader.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TaskURL {
    @JsonProperty(value = "URL")
    private String url;
}
